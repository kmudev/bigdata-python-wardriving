import urllib.request

from bson import json_util, SON
from datetime import datetime
from environs import Env
from flask import Flask, jsonify, request
from flask_pymongo import PyMongo


env = Env()
env.read_env() # read .env file, if one exists
MONGO_PORT = env.int("MONGO_PORT", validate=lambda p: p > 1024, default=27017)
MONGO_DBNAME = env.str("MONGO_DBNAME")
DEBUG_FLAG = env.bool("DEBUG", default=True)

app = Flask(__name__)
app.config['MONGO_URI'] = 'mongodb://mongos' + ':' + str(MONGO_PORT) + '/' + str(MONGO_DBNAME)
app.config['MONGO_DBNAME'] = MONGO_DBNAME
mongo = PyMongo(app)


def convertDateString(dateString):
    return datetime.strptime(dateString, '%a %b %d %H:%M:%S %Y')


@app.route('/')
def hello_world():
   return 'Hello from /\n'

@app.route('/networks', methods=['GET'])
def get_all_networks():
    network = mongo.db.wirelessnw

    output = []

    for doc in network.find():
        output.append({'ssid' : doc['ssid'], 'bssid' : doc['bssid']})

    return jsonify({'result' : output})

@app.route('/stats', methods=['GET'])
def getStat():
    collection = mongo.db.wirelessnw

    WPACount = collection.count_documents({'Privacy': 'WPA'})
    WPA2Count = collection.count_documents({'Privacy': "WPA2"})
    WEPCount = collection.count_documents({'Privacy': "WEP"})
    WPAWPA2Count = collection.count_documents({'Privacy': "WPA2WPA"})
    OPNCount = collection.count_documents({'Privacy': "OPN"})

    result = {'WPACount' : WPACount, 'WPA2Count' : WPA2Count, 'WEPCount' : WEPCount, 'WPA2WPACount' : WPAWPA2Count, 'OPNCount' : OPNCount}

    return jsonify({'result': result})


@app.route('/networks/<ssid>', methods=['GET'])
def get_single_network(ssid):
    network = mongo.db.wirelessnw

    query = network.find_one({'ssid' : ssid})

    if query:
        output = {'ssid' : query['ssid'], 'bssid' : query['bssid']}
    else:
        output = 'No results found'

    return jsonify({'result' : output})

@app.route('/networks', methods=['POST'])
def add_networks():
    collection = mongo.db.wirelessnw

    networks = json_util.loads(str(request.get_json()))

    #Old working method
    #collection.insert(networks)
    #Debugging
    #print(str(networks), flush=True)
    #

    idsToDelete = []
    aggregatedNetworks = []

    for newNetwork in networks:
        existingNetwork = collection.find_one({'BSSID': newNetwork['BSSID']})

        if existingNetwork:

            idsToDelete.append(existingNetwork['_id'])
            #Debugging
            #print("ID to delete: " + str(existingNetwork['_id']), flush=True)

            # List comprehension didn't work for this one for some reason ;(
            for existingClient in existingNetwork['Client']:
                for newClient in newNetwork['Client']:
                    if existingClient['ClientMAC'] == newClient['ClientMAC']:
                        existingClientLastSeen = convertDateString(existingClient['LastSeen'])
                        newClientLastSeen = convertDateString(newClient['LastSeen'])

                        if existingClientLastSeen < newClientLastSeen:
                            existingClient['LastSeen'] = newClient['LastSeen']

            existingMACs = [existingClient['ClientMAC'] for existingClient in existingNetwork['Client']]
            newClients = [newClient for newClient in newNetwork['Client'] if newClient['ClientMAC'] not in existingMACs]

            for client in newClients:
                existingNetwork['Client'].append(client)
            
            newNetwork['Client'] = existingNetwork['Client']

            # Needs refactoring
            exN = existingNetwork['Location']
            newN = newNetwork['Location']

            if exN['Longitude'] != newN['Longitude']:
                if newN['Longitude'] != "Not coordinates available":
                    newNetwork['Location'] = existingNetwork['Location']
            
        aggregatedNetworks.append(newNetwork)

    collection.delete_many({'_id': {"$in": idsToDelete}})
    collection.insert(aggregatedNetworks)

    return jsonify({'result' : 'success'})


if __name__ == '__main__':
     app.run(host="0.0.0.0", port='3000', debug=DEBUG_FLAG)
