#!/bin/bash

COMMAND=$1

if [ -e $COMMAND ]; then
	printf "Use start/stop/restart as arguments to the script\n"
	exit 1
fi

if [ "$COMMAND" == "start" ]; then
	docker-compose up -d
	printf "Waiting 15s for containers to start up.\n"
	sleep 10s
	./config_mongo.sh
	exit
fi

if [ "$COMMAND" == "start_swarm" ]; then
	mkdir data{1..3} config{1..3}
	docker stack deploy --compose-file docker-compose.yml wdr
	printf "Waiting 15s for containers to start up.\n"
	sleep 15s
	./config_mongo.sh swarm
	exit
fi

if [ "$COMMAND" == "restart" ]; then
	docker-compose up -d
	exit
fi

if [ "$COMMAND" == "stop" ]; then
	if [ -e `docker stack ls | grep wdr` ]; then
		docker-compose down
	else
		docker stack rm wdr
	fi

	exit
fi

if [ "$COMMAND" == "prune" ]; then
	sudo rm -rf data{1..3} config{1..3}
	exit
fi

printf "Invalid argument. Use start/stop/restart.\n"
