from os import path

import yaml

from kubernetes import client, config


#load config fom default location
config.load_kube_config()
v1 = client.CoreV1Api()

def listAllPods():
    print("Listing pods with their IPs:")
    ret = v1.list_pod_for_all_namespaces(watch=False)
    for i in ret.items:
        print("%s\t%s\t%s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))

def createDeployment():
    with open(path.join(path.dirname(__file__), "workers-deployment.yaml")) as f:
        dep = yaml.safe_load(f)
        k8s_beta = client.ExtensionsV1beta1Api()
        resp = k8s_beta.create_namespaced_deployment(body=dep, namespace="default")
    print("Deployment created. Status='%s'" % str(resp.status))
