#!/bin/bash
# Configure the mongo cluster

#This works only if using single host swarm. For multi-host setup, refer to other strategy.
if [ "$1" == "swarm" ]; then
	CONFIG_HOST=`docker ps | grep mongocfg1 | awk '{print $1}'`
	SHARD_HOST=`docker ps | grep mongors1n1 | awk '{print $1}'`
	ROUTER_HOST=`docker ps | grep mongos | awk '{print $1}'`
else
	CONFIG_HOST=mongocfg1
	SHARD_HOST=mongors1n1
	ROUTER_HOST=mongos
fi

echo $CONFIG_HOST $SHARD_HOST $ROUTER_HOST

# configure the config servers replica set:
docker exec -it $CONFIG_HOST bash -c "echo 'rs.initiate({_id: \"mongors1conf\",configsvr: true, members: [{ _id : 0, host : \"mongocfg1\" },{ _id : 1, host : \"mongocfg2\" }, { _id : 2, host : \"mongocfg3\" }]})' | mongo"
# check replica set status
docker exec -it $CONFIG_HOST bash -c "echo 'rs.status()' | mongo"
# build the shard replica set
docker exec -it $SHARD_HOST bash -c "echo 'rs.initiate({_id : \"mongors1\", members: [{ _id : 0, host : \"mongors1n1\" },{ _id : 1, host : \"mongors1n2\" },{ _id : 2, host : \"mongors1n3\" }]})' | mongo"
# check shard replica set status
docker exec -it $SHARD_HOST bash -c "echo 'rs.status()' | mongo"
# configure router to use the shards
printf "Waiting for 40s for elections.\n"
sleep 40s
docker exec -it $ROUTER_HOST bash -c "echo 'sh.addShard(\"mongors1/mongors1n1\")' | mongo"
# check shard status
docker exec -it $ROUTER_HOST bash -c "echo 'sh.status()' | mongo"
# create DB
docker exec -it $SHARD_HOST bash -c "echo 'use networksdb' | mongo"
# shard the DB
docker exec -it $ROUTER_HOST bash -c "echo 'sh.enableSharding(\"networksdb\")' | mongo"
# create collection
docker exec -it $SHARD_HOST bash -c "echo 'db.createCollection(\"networksdb.wirelessnw\")' | mongo"
# shard collection on selected sharding field
docker exec -it $ROUTER_HOST bash -c "echo 'sh.shardCollection(\"networksdb.wirelessnw\", {\"Location.Longitude\" : 1})' | mongo"