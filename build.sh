#!/bin/sh

echo "Building docker image for 'backend' service and client."

docker build -t wardriver-backend:latest ./backend
echo "Image 'wardrive-backend:latest' built."

docker build -t wardriver-client:latest ./client
echo "Image 'wardrive-client:latest' built."
