#!/bin/bash

HOST_PORT="${$1:-20175}"
MOBILE_PORT="${$2:-50000}"

#Sets up gpsd and adb
sudo apt update && sudo apt install gpsd gpsd-clients adb -y
sudo systemctl stop gpsd*

#printf "GPSD ready to be run with custom parameters.\nUse 'gpsd -N udp://*:<port_of_your_phone>' to start it and 'gpsmon' to see output.\n"

if [[ -e $(adb devices | grep -i unauthorized) ]]; then
	printf "Your device is not connected or authorized. Connect your phone and enable USB Debugging first.\n"
	exit 1
else
	adb forward tcp:$HOST_PORT tcp:$MOBILE_PORT
	printf "Forwarding is enabled from device port $MOBILE_PORT to host port $HOST_PORT."
	gpsd -b tcp://localhost:$HOST_PORT
	printf "Started gpsd on port $HOST_PORT.\n"