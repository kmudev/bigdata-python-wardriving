import os
import xml.etree.ElementTree as ET

from environs import Env
from flask import Flask, flash, request, redirect, url_for, send_from_directory, render_template, Markup
from json import dumps
from json2html import *
import pygal
from requests import get, post
from werkzeug.utils import secure_filename


env = Env()
env.read_env() # read .env file, if one exists
DEBUG_FLAG = env.bool('DEBUG', default=True)
BACKEND_HOST = env.str('BACKEND_HOST')
BACKEND_PORT = env.str('BACKEND_PORT')                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              

ALLOWED_EXTENSIONS = set(['netxml', 'xml'])
BATCH_SIZE = env.int('BATCH_SIZE', validate=lambda p: p > 0, default=100)

app = Flask(__name__)



    
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/', methods=('GET', 'POST'))
def index():
    return render_template('index.html')


@app.route('/upload', methods=['POST'])
def upload():
    uploadedFiles = request.files.getlist('file[]')
    returnList = []
        
    for netxml in uploadedFiles:
        if netxml and allowed_file(netxml.filename):
            # stringify the netxml
            netxml = netxml.read()
            returnList += parseNetXml(netxml)  # returns a list of dictionaries
        elif not netxml:
            flash(u'No file uploaded. Please select a file below', 'danger')
            return render_template('index.html')
        elif not allowed_file(netxml.filename):
            flash(u'Invalid file extension. Must be a .netxml file.', 'danger')
            return render_template('index.html')
        else:
            flash(u'Error processing file', 'danger')
            return render_template('index.html')

    # add data to JSON array.
    newjson = {"data": returnList}
    newjson = dumps(newjson)

    #debug
    print("Full file parsed.", flush=True)
    #

    # convert JSON to HTML table
    netxml_html = json2html.convert(json=newjson, escape=False,
                                    table_attributes="cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"table table-striped table-bordered table-condensed\" id=\"main\"")  # Create HTML table

    # cut some of the html to make it look normal in datatables
    netxml_html = netxml_html[410:]
    netxml_html = netxml_html[:-26]

    start_table = "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"table table-striped table-bordered table-condensed\" id=\"main\"><thead><tr><th>ESSID</th><th>BSSID</th><th>Channel</th><th>Encryption</th><th>Cipher</th><th>Authenticaton</th><th>DBM</th><th>Location</th><th>Clients</th></tr></thead><tbody>"
    end_table = "</tbody></table>"

    netxml_html = start_table + netxml_html + end_table

    return render_template('upload.html', table=Markup(netxml_html))


def getClients(network, bssid, essid_text):
    clients = network.iter('wireless-client')

    if clients is not None:
        client_info = list()

        for client in clients:
            firstSeen = client.get('first-time')
            lastSeen = client.get('last-time')
            mac = client.find('client-mac')
            manufacturer = client.find('client-manuf')
            if manufacturer is not None:
                client_manufacturer = manufacturer.text
            if mac is not None:
                client_mac = mac.text
                snr = client.find('snr-info')
                if snr is not None:
                    power = client.find('snr-info').find('max_signal_dbm')
                    if power is not None:
                        clientEntry = dict(ClientMAC=client_mac, Manufacturer=client_manufacturer, FirstSeen=firstSeen, LastSeen=lastSeen, ClientPower=power.text)
                        client_info.append(clientEntry)

        return client_info
    else:
        return None


def parseNetXml(kismet_log_file):
    try:
        doc = ET.fromstring(kismet_log_file)
    except IOError:
        print("Can't read the logfile.")
        sys.exit(1)

    total = len(list(doc.iter("wireless-network")))
    num_parsed = 0
    batch_count = 0
    clients = list()

    parsed_list = []

    for network in doc.iter("wireless-network"):

        if batch_count == BATCH_SIZE:
            sendToDatabase(parsed_list[num_parsed-BATCH_SIZE:num_parsed])
            batch_count = 0

        type = network.attrib["type"]
        channel = network.find('channel').text
        bssid = network.find('BSSID').text


        if type == "probe" or channel == "0":
            continue

        encryption = network.iter('encryption')
        privacy = ""
        cipher = ""
        auth = ""
        if encryption is not None:
            for item in encryption:
                if item.text.startswith("WEP"):
                    privacy = "WEP"
                    cipher = "WEP"
                    auth = ""
                    break
                elif item.text.startswith("WPA"):
                    if item.text.endswith("PSK"):
                        auth = "PSK"
                    elif item.text.endswith("AES-CCM"):
                        cipher = "CCMP " + cipher
                    elif item.text.endswith("TKIP"):
                        cipher += "TKIP "
                elif item.text == "None":
                    privacy = "OPN"

        cipher = cipher.strip()

        if cipher.find("CCMP") > -1:
            privacy = "WPA2"

        if cipher.find("TKIP") > -1:
            privacy += "WPA"


        power = network.find('snr-info')
        dbm = ""
        if power is not None:
            dbm = power.find('max_signal_dbm').text

        if int(dbm) > 1:
            dbm = power.find('last_signal_dbm').text

        if int(dbm) > 1:
            dbm = power.find('min_signal_dbm').text

        ssid = network.find('SSID')
        essid_text = ""
        if ssid is not None:
            essid_text = network.find('SSID').find('essid').text

        gps = network.find('gps-info')
        lat, lon = '', ''
        if gps is not None:
            lat = network.find('gps-info').find('min-lat').text
            lon = network.find('gps-info').find('min-lon').text

        data = dict(ESSID=essid_text, BSSID=bssid, Channel=channel, Privacy=privacy,
                    Cipher=cipher, Authenticaiton=auth, DBM=dbm)

        if lat and lon is not None:
            google_map = "https://maps.google.com/maps?q=" + lat + "," + lon + "&ll=" + lat + "," + lon + "&z=17"
            google_map_link = "<a href=\"" + google_map + "\"> Link</a>"

            location = dict(Latitude=lat, Longitude=lon, GoogleMap=google_map_link) # add GPS info
        else:
            not_found = "Not coordinates available"
            location = dict(Latitude=not_found, Longitude=not_found)

        data['Location'] = location

        client_list = getClients(network, bssid, essid_text) # return a dictionary list of clients

        if client_list is not None:
                data['Client'] = client_list # add a client
        else:
            not_found = "No clients found"
            data['Client'] = not_found

        parsed_list.append(data) # add json to list
        num_parsed += 1
        batch_count += 1

    sendToDatabase(parsed_list[num_parsed-batch_count:num_parsed])   
    return parsed_list


def sendToDatabase(parsedNetworks):
    host = "http://" + BACKEND_HOST + ":" + BACKEND_PORT
    endPoint = "/networks"
    url = host + endPoint
    #networksJSON = {'Data': parsedNetworks}

    req = post(url, json=dumps(parsedNetworks))
    
    #Debugging
    #networksJSON = dumps(parsedNetworks)
    #print(str(networksJSON), flush=True)
    print("Parsed networks: " + str(len(parsedNetworks)), flush=True)
    print(req.status_code)
    return req.status_code

@app.route('/global', methods=['GET'])
def globalStats():
    host = "http://" + BACKEND_HOST + ":" + BACKEND_PORT
    endPoint = "/stats"
    url = host + endPoint

    response = get(url).json()
    result = response['result']

    #Debugging
    print(str(result), flush=True)
    #

    wpacount = int(result['WPACount'])
    wpa2count = int(result['WPA2Count'])
    wpawpa2count = int(result['WPA2WPACount'])
    wepcount = int(result['WEPCount'])
    opencount = int(result['OPNCount'])
    fullCount =  opencount + wepcount + wpawpa2count + wpa2count + wpacount

    pie_chart = pygal.Pie()
    pie_chart.title = 'Encryption type (in %)'
    pie_chart.add('WPA2: ' + str(wpa2count), (wpa2count*100)/fullCount)
    pie_chart.add('WPA+WPA2: ' + str(wpawpa2count), (wpawpa2count*100)/fullCount)
    pie_chart.add('WPA: ' + str(wpacount), (wpacount*100)/fullCount)
    pie_chart.add('WEP: ' + str(wepcount), (wepcount*100)/fullCount)
    pie_chart.add('None: ' + str(opencount), (opencount*100)/fullCount)
    pie_data = pie_chart.render_data_uri()

    return render_template("encryption_pie.html", pie_data=pie_data)
    

def main():
    app.run(host="0.0.0.0", port='5000', debug=DEBUG_FLAG)


if __name__ == "__main__": 
    main()
