# Repo hosting the Wardriver implementation.

## Before you start, build the backend container:

1. Clone the repo and `cd` to it.
2. Make the build script executable with `chmod +x build.sh`
3. Run it with `./build.sh`

You're ready to deploy the project locally.

## Use docker-compose or script to deploy the project

1. Run the `./control.sh` script with `start` or `stop` as arguments. 

## How to use 'gpsd'

1. On your machine, run the `./gpsd_init.sh`

Make sure to sync with this tutorial: https://www.jillybunch.com/sharegps/nmea-usb-linux.html
 
